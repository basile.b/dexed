unit u_synmultiguttermarks;

{$I u_defines.inc}

interface

uses
  Classes, SysUtils, Graphics, LCLType, LCLIntf, LCLProc, Controls, ImgList,
  LCLVersion, SynGutterBase, SynEditMiscClasses, SynEditMiscProcs,
  SynEditMarks, SynEditFoldedView, LazSynEditText;

type

  TSynMultiGutterMarks = class(TSynGutterPartBase)
  private
    fColumnCount: Integer;
    fColumnWidth: Integer;
    fImages: TCustomImageList;
    procedure setColumnCount(value: integer);
    procedure setColumnWidth(value: integer);
    procedure setImages(value: TCustomImageList);
  protected
    procedure Init; override;
    function  PreferedWidth: Integer; override;
    // PaintMarks: True, if it has any Mark, that is *not* a bookmark
    function  PaintMarks(aScreenLine: Integer; Canvas : TCanvas; AClip : TRect;
                       var aFirstCustomColumnIdx: integer): Boolean;
    Procedure PaintLine(aScreenLine: Integer; Canvas : TCanvas; AClip : TRect); virtual;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure Paint(Canvas: TCanvas; AClip: TRect; FirstLine, LastLine: integer); override;

    property columnWidth: integer read fColumnWidth write SetColumnWidth;
    property columnCount: integer read fColumnCount write setColumnCount;
    property images: TCustomImageList read fImages write setImages;
  end;

implementation

uses
  SynEdit;

constructor TSynMultiGutterMarks.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TSynMultiGutterMarks.Destroy;
begin
  inherited Destroy;
end;

procedure TSynMultiGutterMarks.Init;
begin
  inherited Init;
end;

function TSynMultiGutterMarks.PreferedWidth: Integer;
begin
  Result := ScaleX(20, 96) * fColumnCount;
end;

procedure TSynMultiGutterMarks.setColumnCount(value: integer);
begin
  if value = fColumnCount then
    exit;
  fColumnCount := value;
  TCustomSynEdit(SynEdit).Gutter.DoAutoSize();
end;

procedure TSynMultiGutterMarks.setColumnWidth(value: integer);
begin
  fColumnWidth := value;
end;

procedure TSynMultiGutterMarks.setImages(value: TCustomImageList);
begin
  fImages := value;
end;

function TSynMultiGutterMarks.PaintMarks(aScreenLine: Integer; Canvas : TCanvas;
  AClip : TRect; var aFirstCustomColumnIdx: integer): Boolean;
var
  markLine: TSynEditMarkLine;
  markRect: TRect;
  mark    : TSynEditMark;
  i, h, w : integer;
  iRange: TLineRange;
begin
  Result := False;
  aFirstCustomColumnIdx := 0;
  aScreenLine := aScreenLine + ToIdx(GutterArea.TextArea.TopLine);
  i := ViewedTextBuffer.DisplayView.ViewToTextIndexEx(aScreenLine, iRange);
  if aScreenLine <> iRange.Top then
    exit;
  if (i < 0) or (i >= TCustomSynEdit(SynEdit).Lines.Count) then
    exit;
  markLine := TCustomSynEdit(SynEdit).Marks.Line[i + 1];
  if markLine = nil then
    exit;

  h := TCustomSynEdit(SynEdit).LineHeight;

  w := ScaleX(20, 96);
  for i := 0 to markLine.Count - 1 do
  begin
    mark := markLine[i];
    if (not mark.visible) or (mark.ImageIndex = -1) or (mark.ImageList = nil) then
      continue;
    markRect := Rect(AClip.Left + 2 + mark.Column * w, AClip.Top, AClip.Left + 500, AClip.Top + h);
    mark.ImageList.draw(canvas, markRect.Left, markRect.Top, mark.ImageIndex);
    Result := true;
  end;
end;

procedure TSynMultiGutterMarks.PaintLine(aScreenLine: Integer; Canvas: TCanvas; AClip: TRect);
var
  aGutterOffs: Integer;
begin
  aGutterOffs := 0;
  PaintMarks(aScreenLine, Canvas, AClip, aGutterOffs);
end;

procedure TSynMultiGutterMarks.Paint(Canvas : TCanvas; AClip : TRect; FirstLine, LastLine : integer);
var
  i: integer;
  LineHeight: Integer;
  rcLine: TRect;
begin
  if not Visible then exit;
  if MarkupInfo.Background <> clNone then
    Canvas.Brush.Color := MarkupInfo.Background
  else
    Canvas.Brush.Color := Gutter.Color;
  LCLIntf.SetBkColor(Canvas.Handle, TColorRef(Canvas.Brush.Color));

  rcLine := AClip;
  rcLine.Bottom := rcLine.Top;
  if (LastLine >= FirstLine) then
  begin
    LineHeight := TCustomSynEdit(SynEdit).LineHeight;
    for i := FirstLine to LastLine do begin
      rcLine.Top := rcLine.Bottom;
      rcLine.Bottom := Min(AClip.Bottom, rcLine.Top + LineHeight);
      PaintLine(i, Canvas, rcLine);
    end;
  end;
end;

end.

end.

