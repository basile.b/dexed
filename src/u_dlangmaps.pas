unit u_dlangmaps;

{$I u_defines.inc}

interface


type

  (**
   * Perfect static hash-map that detects the D2 "special" keywords such as
   * __LINE__ or __FILE__.
   *)
  specialKeywordsMap = record
  private
    const fWords: array [0..15] of string =
    (
      '__VERSION__', '', '__FILE_FULL_PATH__', '__TIME__', '__FILE__', '__VENDOR__',
      '', '__DATE__', '__FUNCTION__', '__LINE__', '__EOF__', '__MODULE__',
      '__PRETTY_FUNCTION__', '', '', '__TIMESTAMP__'
    );
    const fHasEntry: array [0..15] of boolean =
    (
      true, false, true, true, true, true, false, true, true, true, true, true,
      true, false, false, true
    );
    const fCoeffs: array[0..255] of Byte =
    (
      162, 105, 225, 180, 180, 12, 125, 73, 237, 109, 3, 67, 160, 192, 35, 42,
      131, 170, 41, 106, 103, 53, 105, 74, 29, 64, 247, 248, 184, 146, 172, 142,
      239, 232, 158, 168, 29, 243, 40, 241, 255, 85, 184, 38, 44, 242, 193, 222,
      86, 131, 181, 101, 161, 209, 115, 124, 91, 118, 188, 67, 172, 115, 24, 221,
      142, 99, 17, 30, 231, 80, 185, 182, 185, 55, 4, 23, 152, 63, 126, 37, 158,
      36, 28, 235, 65, 220, 243, 62, 169, 129, 127, 76, 149, 232, 21, 119, 134,
      144, 20, 89, 103, 65, 109, 12, 95, 200, 41, 14, 52, 25, 56, 228, 4, 227,
      86, 113, 77, 158, 46, 246, 90, 25, 210, 214, 149, 219, 219, 27, 95, 203,
      43, 21, 191, 94, 216, 113, 100, 222, 245, 224, 127, 174, 214, 44, 78, 89,
      213, 184, 73, 77, 236, 131, 46, 90, 58, 171, 34, 215, 201, 104, 138, 251,
      54, 103, 75, 235, 12, 149, 49, 19, 128, 72, 138, 224, 73, 174, 151, 50,
      152, 32, 135, 238, 132, 34, 3, 230, 201, 166, 31, 119, 50, 155, 125, 103,
      133, 250, 253, 218, 48, 167, 207, 107, 235, 53, 214, 213, 49, 8, 13, 247,
      37, 251, 21, 43, 34, 108, 162, 160, 133, 199, 169, 218, 189, 1, 128, 17,
      67, 186, 55, 2, 23, 23, 133, 114, 240, 176, 124, 127, 217, 231, 129, 220,
      250, 17, 136, 92, 191, 172, 16, 137, 23, 109, 37, 191, 74, 218
    );
    class function hash(const w: string): Byte; static; {$IFNDEF DEBUG}inline;{$ENDIF}
  public
    class function match(const w: string): boolean; static; {$IFNDEF DEBUG}inline;{$ENDIF}
  end;

  (**
   * Perfect static hash-map that detects the 'straight' D2 keywords plus a few
   * exception for the library types related to the strings and registry-wide integers.
   *)
  keywordsMap = record
  private
  {
        rendered on 2025-Mar-10 11:20:03.8930671 by IsItThere.
         - PRNG seed: 0
         - map length: 512
         - case sensitive: true
  }

  const fWords: array [0..511] of string =
    ('', '', '', '', 'cdouble', '', '', '', 'creal', '', 'abstract', '', 'ulong', 'uint', '', '', '', '', 'delete', '', '', '', '', '', '', 'while', '', '', '', '', '', 'goto', 'void', '', '', 'ptrdiff_t', '', '', 'extern', '', '', '', '', '__traits', '', '', '', 'ifloat', '', '', 'dchar', 'pure', '', '', '', 'template', '', 'auto', '', 'cast', '', '', 'synchronized', '', '', '', 'union', '', '', '', '', '', '', '', '__parameters', '', 'finally', '', '', 'this', '__rvalue', '', '', 'mixin', 'throw', '', '', '', '', 'class', '', '', '', '', '', '', '', '', '', 'false', '', '', '', '', '', '', '', '', '', '', '', 'continue', '', 'import', '', '', '', 'break', '', '', 'immutable', '', 'cfloat', '', '', 'case', '', 'enum', '', '', 'override', 'public', '', 'cent', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'inout', '', '', '', 'in', '', '', '', '', 'interface', 'true', '', '', 'catch', '', 'string', '', '', 'delegate', '', 'else', '', 'if', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'short', '', '', '', '', '', '', '', '', '', 'const', '', '', '', 'wstring', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'super', '', '', 'package', '', '', '', '__gshared', '', '', '', '', '', '', '', '', '', 'is', '', '', '', '', 'scope', '', '', '__vector', '', 'foreach_reverse', '', '', '', '', 'do', '', '', '', '', '', 'unittest', '', 'switch', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'static', '', '', 'dstring', 'null', '', '', 'byte', '', 'for', 'nothrow', '', '', 'debug', 'return', '', '', '', 'align', '', '', '', '', '', '', 'module', '', 'ucent', 'long', 'int', '', '', '', 'final', '', '', '', '', '', 'shared', '', '', '', '', '', '', '', '', '', '', 'protected', '', 'lazy', '', '', '', '', '', 'private', '', 'invariant', '', '', '', '', '', 'size_t', '', '', 'asm', '', '', 'double', '', '', '', 'real', '', '', '', '', '', '', '', '', '', '', '', '', 'new', '', 'volatile', '', 'alias', '', '', '', 'foreach', '', '', '', '', '', '', '', '', '', '', '', 'ushort', 'ref', 'default', 'typeid', 'assert', '', '', '', '', '', '', '', '', '', 'export', 'char', '', '', '', '', '', '', 'struct', '', '', '', '', '', '', '', '', 'idouble', '', '', '', 'ireal', '', '', '', '', '', '', 'bool', 'typeof', '', '', '', 'pragma', '', '', 'with', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'deprecated', '', '', '', '', 'version', 'try', '', '', '', '', 'wchar', '', 'function', '', 'float', '', '', '', '', '', '', '', '', '', '', '', 'ubyte', '', '', '', '', 'out', '', '', '');

  const fHasEntry: array [0..511] of boolean =
    (false, false, false, false, true, false, false, false, true, false, true, false, true, true, false, false, false, false, true, false, false, false, false, false, false, true, false, false, false, false, false, true, true, false, false, true, false, false, true, false, false, false, false, true, false, false, false, true, false, false, true, true, false, false, false, true, false, true, false, true, false, false, true, false, false, false, true, false, false, false, false, false, false, false, true, false, true, false, false, true, true, false, false, true, true, false, false, false, false, true, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, true, false, true, false, false, false, true, false, false, true, false, true, false, false, true, false, true, false, false, true, true, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, true, false, false, false, false, true, true, false, false, true, false, true, false, false, true, false, true, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, true, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, true, false, false, false, true, false, false, false, false, false, false, false, false, false, true, false, false, false, false, true, false, false, true, false, true, false, false, false, false, true, false, false, false, false, false, true, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, true, true, false, false, true, false, true, true, false, false, true, true, false, false, false, true, false, false, false, false, false, false, true, false, true, true, true, false, false, false, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, true, false, true, false, false, false, false, false, true, false, true, false, false, false, false, false, true, false, false, true, false, false, true, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, true, false, true, false, true, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, true, true, true, true, true, false, false, false, false, false, false, false, false, false, true, true, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, true, false, false, false, true, false, false, false, false, false, false, true, true, false, false, false, true, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, true, true, false, false, false, false, true, false, true, false, true, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, true, false, false, false);

  const fCoeffs: array [0..255] of Byte =
    (149, 149, 209, 70, 128, 54, 131, 42, 163, 99, 62, 99, 162, 96, 41, 68, 64, 176, 136, 67, 36, 232, 177, 99, 114, 106, 120, 83, 85, 249, 51, 53, 48, 55, 217, 168, 129, 86, 247, 153, 11, 47, 7, 88, 69, 131, 73, 188, 111, 154, 128, 152, 194, 99, 121, 84, 28, 83, 247, 175, 238, 255, 237, 177, 37, 205, 174, 140, 119, 108, 68, 1, 113, 102, 208, 214, 123, 137, 223, 57, 95, 182, 228, 85, 57, 41, 133, 64, 9, 201, 112, 156, 192, 173, 209, 166, 66, 61, 163, 143, 137, 240, 106, 91, 156, 68, 10, 100, 11, 116, 88, 139, 63, 240, 65, 193, 174, 195, 200, 62, 255, 243, 38, 238, 63, 128, 80, 248, 104, 7, 154, 154, 254, 221, 1, 238, 26, 252, 2, 169, 227, 130, 99, 240, 52, 248, 250, 72, 216, 222, 22, 227, 139, 187, 59, 217, 118, 119, 63, 220, 235, 55, 154, 87, 10, 171, 249, 239, 254, 232, 63, 174, 120, 245, 238, 228, 152, 175, 240, 174, 18, 95, 239, 185, 227, 226, 186, 161, 234, 227, 29, 152, 90, 54, 69, 136, 192, 252, 29, 237, 62, 164, 45, 151, 187, 68, 165, 60, 102, 146, 15, 97, 207, 59, 99, 48, 24, 53, 225, 174, 175, 183, 215, 24, 234, 33, 31, 255, 60, 128, 31, 89, 63, 3, 105, 233, 191, 6, 181, 198, 46, 7, 164, 17, 90, 45, 93, 185, 254, 17, 194, 218, 197, 194, 85, 71);
    class function hash(const w: string): Word; static; {$IFNDEF DEBUG}inline;{$ENDIF}
  public
    class function match(const w: string): boolean; static; {$IFNDEF DEBUG}inline;{$ENDIF}
  end;


implementation

{$IFDEF DEBUG}{$PUSH}{$R-}{$ENDIF}
class function specialKeywordsMap.hash(const w: string): Byte;
var
  i: integer;
begin
  Result := 0;
  for i := 1 to length(w) do
    Result += fCoeffs[Byte(w[i])];
  Result := Result and $F;
end;
{$IFDEF DEBUG}{$POP}{$ENDIF}

class function specialKeywordsMap.match(const w: string): boolean;
var
  h: Byte;
begin
  result := false;
  if (length(w) < 7) or (length(w) > 19) then
    exit;
  h := hash(w);
  if fHasEntry[h] then
    result := fWords[h] = w;
end;

{$IFDEF DEBUG}{$PUSH}{$R-}{$ENDIF}
class function keywordsMap.hash(const w: string): Word;
var
  i: integer;
begin
  Result := 0;
  for i := 1 to length(w) do
    Result += fCoeffs[Byte(w[i])];
  Result := Result and $1FF;
end;
{$IFDEF DEBUG}{$POP}{$ENDIF}

class function keywordsMap.match(const w: string): boolean;
var
  h: Word;
begin
  result := false;
  if (length(w) < 2) or (length(w) > 15) then
    exit;
  h := hash(w);
  if fHasEntry[h] then
    result := fWords[h] = w;
end;

end.

